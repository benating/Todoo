//
//  Item.swift
//  Todoo
//
//  Created by Bernard Ting on 03/01/2018.
//  Copyright © 2018 Bernard Ting. All rights reserved.
//

import Foundation

class Item: Codable {
    var title : String = ""
    var isDone : Bool = false
}
